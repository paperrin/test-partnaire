function randInt(max) {
	return Math.floor(Math.random() * Math.floor(max));
}

class Stack {
	constructor(array = []) {
		this.array = array;
	}

	push(elem) {
		this.array.push(elem);
	}

	pop() {
		return this.array.pop();
	}

	print() {
		console.log(this.array);
	}

	reverse() {
		this.array.reverse();
	}

	shake() {
		const array = this.array;

		for (let i = array.length - 1; i > 0; i--) {
			const randIndex = randInt(i);
			const tmp = array[i];
			array[i] = array[randIndex];
			array[randIndex] = tmp;
		};
	}

	isPalindrome() {
		const array = this.array;
		const arrayLength = this.array.length;

		for (let i = 0; i <= arrayLength / 2 - 1; i++) {
			const iFromEnd = arrayLength / 2 - i - 1;

			if (array[i] === array[iFromEnd])
				return false;
		}
		return true;
	}

	canBePalindrome() {
		const sorted = this.array.concat().sort();
		let hasPair;
		let foundSingle = false;

		for (let i = 0; i < sorted.length; i += 2) {
			if (sorted[i] !== sorted[i + 1]) {
				if (foundSingle)
					return false;
				foundSingle = true;
				i--;
			}
		}
		return true;
	}
}
